# QA task for Lingoda

Write a basic browser automation framework to validate a Polymer website. 
The focus should be on the interaction with the browser.

The Web Application under test http://todomvc.com/

1. The first step should be to load the website, click within the JavaScript tab, and select the Polymer link.
2. The second step should be: Add two Todo items
3. Bonus: (optional stretch goal): Edit the content of the second Todo item
