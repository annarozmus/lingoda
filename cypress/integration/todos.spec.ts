import {Elements} from "../support/page-objects/elements.page";

const selector = new Elements();
const cat = 'Feed the cat';

describe('Todo list', () => {
    beforeEach(function () {
        cy.visit('/');
    });

    it('Adds two todo items and changes the last one', () => {

        cy.log('Going to todo lists');
        selector.appLists.invoke('attr', 'style', 'display: block');
        selector.jsList.contains('Polymer').click();
        cy.url().should('include', '/examples/polymer/index.html');

        cy.log('Adding two items');
        selector.todoList.should('have.length', 0);
        selector.newTodo.type('Do the laundry{enter}');
        selector.newTodo.type('Go running{enter}');
        selector.todoList.should('have.length', 2);

        cy.log('Editing the second item');
        selector.todoList.last().find('label').dblclick();
        selector.todoList.last().find('input.edit').clear().type(cat + '{enter}');
        selector.todoList.last().should('have.text', cat);

    });
});