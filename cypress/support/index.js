import './commands'
import './page-objects/elements.page'

// It prevents failing tests because of app not Cypress errors
// found here https://docs.cypress.io/guides/references/error-messages
Cypress.on('uncaught:exception', () => {
    return false;
});