export class Elements {
    get appLists() {
        return cy.get('iron-pages.app-lists');
    }

    get jsList() {
        return cy.get('div.js-app-list li');
    }

    get newTodo() {
        return cy.get('#new-todo');
    }

    get todoList() {
        return cy.get('ul.todo-list li');
    }
}